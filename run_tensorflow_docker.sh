#!/bin/bash

sudo nvidia-docker run -tid --name=tensorflow -p 8888:8888 -p 6006:6006 -v /home/learner/docker:/home/docker tensorflow/tensorflow:latest-devel-gpu
sudo nvidia-docker start tensorflow
sudo nvidia-docker exec -it tensorflow /usr/local/bin/jupyter notebook --allow-root --notebook-dir=/home/docker